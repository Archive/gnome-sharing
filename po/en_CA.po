# English/Canada translation of gnome-sharing.
# Copyright (C) 2004 Adam Weinberger and the GNOME Foundation
# This file is distributed under the same licence as the gnome-sharing package.
# Adam Weinberger <adamw@gnome.org>, 2004.
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: gnome-sharing\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-05-01 13:16-0400\n"
"PO-Revision-Date: 2004-05-01 13:17-0400\n"
"Last-Translator: Adam Weinberger <adamw@gnome.org>\n"
"Language-Team: Canadian English <adamw@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#: desktop/data/gnome-desktop-sharing.schemas.in.h:1
msgid "Check this to enable desktop sharing in GNOME"
msgstr "Check this to enable desktop sharing in GNOME"

#: desktop/data/gnome-desktop-sharing.schemas.in.h:2
msgid "Password to connect to desktop sharing"
msgstr "Password to connect to desktop sharing"

#: desktop/data/gnome-desktop-sharing.schemas.in.h:3
msgid ""
"Set this to the password clients should use to connect to your desktop "
"remotely"
msgstr ""
"Set this to the password clients should use to connect to your desktop "
"remotely"

#: desktop/data/gnome-desktop-sharing.schemas.in.h:4
msgid "Whether to enable desktop sharing"
msgstr "Whether to enable desktop sharing"

#: desktop/preferences/interface.c:49 desktop/preferences/interface.c:64
#: desktop/server/tray.c:167
msgid "Desktop Sharing"
msgstr "Desktop Sharing"

#: desktop/preferences/interface.c:73
msgid "_Share my desktop with others"
msgstr "_Share my desktop with others"

#: desktop/preferences/interface.c:79
msgid "Enable this option to allow others to view your desktop"
msgstr "Enable this option to allow others to view your desktop"

#: desktop/preferences/interface.c:94
msgid ""
"The following information will need to be supplied by the user connecting to "
"your desktop"
msgstr ""
"The following information will need to be supplied by the user connecting to "
"your desktop"

#: desktop/preferences/interface.c:105
msgid "Password"
msgstr "Password"

#: desktop/preferences/interface.c:123
msgid "Automatically accept clients"
msgstr "Automatically accept clients"

#: desktop/preferences/support.c:60 desktop/preferences/support.c:85
#, c-format
msgid "Couldn't find pixmap file: %s"
msgstr "Couldn't find pixmap file: %s"

#: desktop/server/eggtrayicon.c:118
msgid "Orientation"
msgstr "Orientation"

#: desktop/server/eggtrayicon.c:119
msgid "The orientation of the tray."
msgstr "The orientation of the tray."

#: desktop/server/tray.c:66
msgid "/_Preferences"
msgstr "/_Preferences"

#: desktop/server/tray.c:67
msgid "/_Clients"
msgstr "/_Clients"

#: desktop/server/tray.c:69
msgid "/_About"
msgstr "/_About"

#: desktop/server/tray.c:70
msgid "/_Quit"
msgstr "/_Quit"

#: desktop/server/tray.c:122
msgid "Your desktop is viewable to others"
msgstr "Your desktop is viewable to others"

#: desktop/server/tray.c:169
msgid "Remote Desktop Sharing"
msgstr "Remote Desktop Sharing"

#: desktop/server/tray.c:172
msgid "This is an untranslated version of GNOME Network Information"
msgstr "This is an untranslated version of GNOME Network Information"

#: desktop/server/tray.c:407
msgid "Clients currently viewing your desktop"
msgstr "Clients currently viewing your desktop"

#: desktop/server/tray.c:429
msgid "Disconnect"
msgstr "Disconnect"

#: desktop/server/x11vnc.c:346
#, c-format
msgid "Host %s is no longer viewing your desktop"
msgstr "Host %s is no longer viewing your desktop"

#: desktop/server/x11vnc.c:419
#, c-format
msgid "Host %s is trying to view your desktop, allow them?"
msgstr "Host %s is trying to view your desktop. Allow them?"

#: desktop/server/x11vnc.c:445
#, c-format
msgid "Host %s is now viewing your desktop"
msgstr "Host %s is now viewing your desktop"

#: files/Nautilus_View_sharing_properties.server.in.in.h:1
msgid "Nautilus Sharing Properties view"
msgstr "Nautilus Sharing Properties view"

#: files/Nautilus_View_sharing_properties.server.in.in.h:2
msgid "Sharing"
msgstr "Sharing"

#: files/Nautilus_View_sharing_properties.server.in.in.h:3
msgid "Sharing Properties content view component"
msgstr "Sharing Properties content view component"

#: files/nautilus-sharing-properties-view.c:151
msgid "loading..."
msgstr "loading..."

#: files/nautilus-sharing-properties-view.c:164
msgid "URI currently displayed"
msgstr "URI currently displayed"

#: files/sharing-properties-view.c:119
msgid "Can't detect any web server. You need one for web sharing"
msgstr "Can't detect any web server. You need one for web sharing"

#: files/sharing-properties-view.glade.h:1
msgid "      "
msgstr "      "

#: files/sharing-properties-view.glade.h:2
msgid "Audio Properties"
msgstr "Audio Properties"

#: files/sharing-properties-view.glade.h:3
msgid "Sharing for Web"
msgstr "Sharing for Web"

