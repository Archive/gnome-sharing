# translation of mk.po to Macedonian
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
#
# Арангел Ангов <ufo@linux.net.mk>, 2005.
# Arangel Angov <ufo@linux.net.mk>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: mk\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-08-12 11:57+0200\n"
"PO-Revision-Date: 2006-08-12 13:03+0200\n"
"Last-Translator: Arangel Angov <ufo@linux.net.mk>\n"
"Language-Team: Macedonian <ossm-members@hedona.on.net.mk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.2\n"

#: ../desktop/data/gnome-desktop-sharing.schemas.in.h:1
msgid "Check this to enable desktop sharing in GNOME"
msgstr "Обележете го ова за да вклучите споделување на работната површина во Гном"

#: ../desktop/data/gnome-desktop-sharing.schemas.in.h:2
msgid "Password to connect to desktop sharing"
msgstr "Лозинка за врзување на споделената работна површина"

#: ../desktop/data/gnome-desktop-sharing.schemas.in.h:3
msgid ""
"Set this to the password clients should use to connect to your desktop "
"remotely"
msgstr ""
"Поставете ја лозинката што ќе ја користат далечните компјутери за да се "
"врзуваат на Вашата работна површина"

#: ../desktop/data/gnome-desktop-sharing.schemas.in.h:4
msgid "Whether to enable desktop sharing"
msgstr "Дали да вклучам споделување на работната површина"

#: ../desktop/preferences/interface.c:49 ../desktop/preferences/interface.c:64
#: ../desktop/server/tray.c:167
msgid "Desktop Sharing"
msgstr "Споделување на работната површина"

#: ../desktop/preferences/interface.c:73
msgid "_Share my desktop with others"
msgstr "_Сподели ја мојата работна површина со други"

#: ../desktop/preferences/interface.c:79
msgid "Enable this option to allow others to view your desktop"
msgstr ""
"Вклучете ја оваа опција за да дозволите други да ја гледаат Вашата работна "
"површина"

#: ../desktop/preferences/interface.c:94
msgid ""
"The following information will need to be supplied by the user connecting to "
"your desktop"
msgstr ""
"Следниве информации треба да бидат внесени од страна на корисникот што се "
"врзува на Вашата работна површина"

#: ../desktop/preferences/interface.c:105
msgid "Password"
msgstr "Лозинка"

#: ../desktop/preferences/interface.c:123
msgid "Automatically accept clients"
msgstr "Автоматски прифаќај клиенти"

#: ../desktop/preferences/support.c:60 ../desktop/preferences/support.c:85
#, c-format
msgid "Couldn't find pixmap file: %s"
msgstr "Не можам да ја најдам pixmap датотеката: %s"

#: ../desktop/server/eggtrayicon.c:118
msgid "Orientation"
msgstr "Ориентација"

#: ../desktop/server/eggtrayicon.c:119
msgid "The orientation of the tray."
msgstr "Ориентацијата на просторот за спуштање на програми."

#: ../desktop/server/tray.c:66
msgid "/_Preferences"
msgstr "/_Преференци"

#: ../desktop/server/tray.c:67
msgid "/_Clients"
msgstr "/_Клиенти"

#: ../desktop/server/tray.c:69
msgid "/_About"
msgstr "/_За"

#: ../desktop/server/tray.c:70
msgid "/_Quit"
msgstr "/_Излез"

#: ../desktop/server/tray.c:122
msgid "Your desktop is viewable to others"
msgstr "Вашата работна повришна можат да ја гледаат и други"

#: ../desktop/server/tray.c:169
msgid "Remote Desktop Sharing"
msgstr "Далечно споделување на работната површина"

#: ../desktop/server/tray.c:172
msgid "This is an untranslated version of GNOME Network Information"
msgstr "Ова е непреведена верзија на мрежните информации за Гном"

#: ../desktop/server/tray.c:407
msgid "Clients currently viewing your desktop"
msgstr "Клиенти кои што ја гледаат Вашата работна површина"

#: ../desktop/server/tray.c:429
msgid "Disconnect"
msgstr "Прекини врска"

#: ../desktop/server/x11vnc.c:346
#, c-format
msgid "Host %s is no longer viewing your desktop"
msgstr "Хостот %s повеќе не ја гледа Вашата работна површина"

#: ../desktop/server/x11vnc.c:419
#, c-format
msgid "Host %s is trying to view your desktop, allow them?"
msgstr "Хостот %s се обидува да ја гледа Вашата работна површина, да му дозволам?"

#: ../desktop/server/x11vnc.c:445
#, c-format
msgid "Host %s is now viewing your desktop"
msgstr "Хостот %s ја гледа Вашата работна површина"

#: ../files/Nautilus_View_sharing_properties.server.in.in.h:1
msgid "Nautilus Sharing Properties view"
msgstr "Поглед на наутилус за својствата за делење"

#: ../files/Nautilus_View_sharing_properties.server.in.in.h:2
msgid "Sharing"
msgstr "Делење"

#: ../files/Nautilus_View_sharing_properties.server.in.in.h:3
msgid "Sharing Properties content view component"
msgstr "Компонента за поглед на својствата на споделената содржина"

#: ../files/nautilus-sharing-properties-view.c:151
msgid "loading..."
msgstr "вчитувам..."

#: ../files/nautilus-sharing-properties-view.c:164
msgid "URI currently displayed"
msgstr "Адресата е прикажана"

#: ../files/sharing-properties-view.c:119
msgid "Can't detect any web server. You need one for web sharing"
msgstr ""
"Не можам да го пронајдам веб серверот. За споделување потребен Ви е веб "
"сервер"

#: ../files/sharing-properties-view.glade.h:1
msgid "      "
msgstr "      "

#: ../files/sharing-properties-view.glade.h:2
msgid "Audio Properties"
msgstr "Аудио својства"

#: ../files/sharing-properties-view.glade.h:3
msgid "Sharing for Web"
msgstr "Споделување за веб"

