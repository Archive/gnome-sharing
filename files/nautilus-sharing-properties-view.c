/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/*
 * Copyright (C) 2002 Thomas Vander Stichele
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Thomas Vander Stichele <thomas at apestaart dot org>
 */

#include <config.h>

#include <gtk/gtkvbox.h>
#include <gtk/gtklabel.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomevfs/gnome-vfs-async-ops.h>
#include <eel/eel-gnome-extensions.h>
#include <string.h>

#include "sharing-properties-view.h"
#include "nautilus-sharing-properties-view.h"

/* from libmain.c */
#include <bonobo.h>
#include <bonobo-activation/bonobo-activation.h>

#define VIEW_IID    "OAFIID:Nautilus_Sharing_Properties_View"

struct NautilusSharingPropertiesViewDetails {
	char *location;
	SharingPropertiesView *view;
	GtkWidget *vbox;
	GtkWidget *resolution;
};

enum {
	PROP_URI,
};

/* gotten from libmain.c */
static CORBA_Object
sharing_shlib_make_object (PortableServer_POA poa,
			 const char *iid,
			 gpointer impl_ptr,
			 CORBA_Environment *ev)
{
	NautilusSharingPropertiesView *view;

	if (strcmp (iid, VIEW_IID) != 0) {
		return CORBA_OBJECT_NIL;
	}

	view = NAUTILUS_SHARING_PROPERTIES_VIEW (g_object_new (NAUTILUS_TYPE_SHARING_PROPERTIES_VIEW, NULL));

	bonobo_activation_plugin_use (poa, impl_ptr);

	return CORBA_Object_duplicate (BONOBO_OBJREF (view), ev);
}

static const BonoboActivationPluginObject sharing_plugin_list[] = {
	{ VIEW_IID, sharing_shlib_make_object },
	{ NULL }
};

const BonoboActivationPlugin Bonobo_Plugin_info = {
	sharing_plugin_list,
	"Nautilus Sharing Properties Page"
};

static void
nautilus_sharing_properties_view_finalize (GObject *object)
{
	NautilusSharingPropertiesView *view;

	view = NAUTILUS_SHARING_PROPERTIES_VIEW (object);

	if (view->details->view != NULL)
		sharing_properties_view_dispose (view->details->view);
	g_free (view->details);

	G_OBJECT_CLASS (object)->finalize (object);
}

static void
load_location (NautilusSharingPropertiesView *view,
	       const char *location)
{
	g_assert (NAUTILUS_IS_SHARING_PROPERTIES_VIEW (view));
	g_assert (location != NULL);

	sharing_properties_view_load_location (view->details->view, location);
}

static void
get_property (BonoboPropertyBag *bag,
	      BonoboArg         *arg,
	      guint              arg_id,
	      CORBA_Environment *ev,
	      gpointer           user_data)
{
	NautilusSharingPropertiesView *view = user_data;

	if (arg_id == PROP_URI) {
		BONOBO_ARG_SET_STRING (arg, view->details->location);
	}
}

static void
set_property (BonoboPropertyBag *bag,
	      const BonoboArg   *arg,
	      guint              arg_id,
	      CORBA_Environment *ev,
	      gpointer           user_data)
{
	NautilusSharingPropertiesView *view = user_data;

	if (arg_id == PROP_URI) {
		load_location (view, BONOBO_ARG_GET_STRING (arg));
	}
}

static void
nautilus_sharing_properties_view_class_init (NautilusSharingPropertiesViewClass *class)
{
	G_OBJECT_CLASS (class)->finalize = nautilus_sharing_properties_view_finalize;
}

static void
nautilus_sharing_properties_view_init (NautilusSharingPropertiesView *view)
{
	BonoboPropertyBag *pb;

	view->details = g_new0 (NautilusSharingPropertiesViewDetails, 1);
	view->details->view = sharing_properties_view_new ();

	view->details->vbox = sharing_properties_view_get_widget (view->details->view);
	view->details->resolution = gtk_label_new (_("loading..."));

	gtk_box_pack_start (GTK_BOX (view->details->vbox),
			    view->details->resolution,
			    FALSE, TRUE, 2);

	gtk_widget_show (view->details->vbox);

	bonobo_control_construct (BONOBO_CONTROL (view), view->details->vbox);

	pb = bonobo_property_bag_new (get_property, set_property,
				      view);
	bonobo_property_bag_add (pb, "URI", 0, BONOBO_ARG_STRING,
				 NULL, _("URI currently displayed"), 0);
	bonobo_control_set_properties (BONOBO_CONTROL (view),
				       BONOBO_OBJREF (pb), NULL);
	bonobo_object_release_unref (BONOBO_OBJREF (pb), NULL);
}

BONOBO_TYPE_FUNC (NautilusSharingPropertiesView, BONOBO_TYPE_CONTROL, nautilus_sharing_properties_view);

