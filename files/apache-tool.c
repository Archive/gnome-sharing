#include <glib.h>
#include <glibtop.h>
#include <glibtop/union.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "apache-tool.h"

#define APACHE_CMD		"apache"
#define APACHE_PUBLIC_HTML	"public_html"

gboolean
apache_is_running (void)
{
	unsigned         *ptr, pid, i;
	glibtop_proclist  proclist;
	glibtop_union     data;

	glibtop_init ();

	ptr = glibtop_get_proclist (&proclist, 0, 0);

	if (!ptr)
		return FALSE;

	g_free (ptr);

	for (i = 0; i < proclist.number; i++) {
		pid = ptr [i];
		
		glibtop_get_proc_state (&data.proc_state, pid);
		if (g_ascii_strcasecmp (data.proc_state.cmd, APACHE_CMD) == 0)
			return TRUE;
	}

	return FALSE;
}

gboolean
apache_share_file (gchar *path)
{
	gchar *public_html;
	gchar *symlink_path;
	
	g_return_val_if_fail (g_file_test (path, G_FILE_TEST_EXISTS), FALSE);

	/* Get user_home dir */
	public_html = g_strdup_printf ("%s/%s", g_get_home_dir (), APACHE_PUBLIC_HTML);

	if (!g_file_test (public_html, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)) {
		/* Create */
		mkdir (public_html, (mode_t) 0755);
	}

	symlink_path = g_strdup_printf ("%s/%s", public_html , strrchr (path, '/'));
	symlink (path, symlink_path);

	return TRUE;
}
