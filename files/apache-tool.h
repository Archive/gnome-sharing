#ifndef __APACHE_TOOL_H__
#define __APACHE_TOOL_H__

#include <glib.h>

gboolean apache_is_running (void);
gboolean apache_share_file (gchar *path);

#endif
