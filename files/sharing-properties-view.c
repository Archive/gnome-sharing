/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/*
 * Copyright (C) 2002 James Willcox
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *	James Willcox <jwillcox@gnome.org>
 *	Thomas Vander Stichele <thomas at apestaart dot org>
 */

#include <config.h>

#include <gtk/gtkentry.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkwindow.h>
#include <glade/glade-xml.h>
#include <libgnome/gnome-macros.h>
#include <eel/eel-gnome-extensions.h>
#include <string.h>

#include "i18n-support.h"
#include "sharing-properties-view.h"
#include "apache-tool.h"

struct SharingPropertiesView {
	char      *location;
	GtkWidget *vbox;
	GtkWidget *web_chk;
	GtkWidget *web_lbl;
};

void sharing_web_toggled (GtkToggleButton *togglebutton, gpointer user_data);


/* private function */
void
sharing_web_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	g_return_if_fail (user_data != NULL);
	/* Only for local files TODO: better way to do it */
	g_return_if_fail (g_ascii_strncasecmp ("file://", (gchar *) user_data, 7) == 0);

	if (gtk_toggle_button_get_active (togglebutton)) {
		apache_share_file ((gchar *) user_data + 7);
	} else {
		/* Remove the symlink if exists */
	}
}

/* public functions */
void
sharing_properties_view_dispose (SharingPropertiesView *view)
{
	g_free (view->location);

	g_free (view);
}

void
sharing_properties_view_load_location (SharingPropertiesView *view,
				       const char *location)
{
	g_signal_connect (GTK_OBJECT (view->web_chk), "toggled",
			  G_CALLBACK (sharing_web_toggled), g_strdup (location));
}

GtkWidget*
sharing_properties_view_get_widget (SharingPropertiesView *view)
{
	return view->vbox;
}

SharingPropertiesView*
sharing_properties_view_new ()
{
	SharingPropertiesView *view;
	GladeXML *xml;

	view = g_new0 (SharingPropertiesView, 1);

	xml = glade_xml_new (DATADIR_UNINST
			     "/sharing-properties-view/sharing-properties-view.glade",
			     "content", NULL);
	if (!xml)
		xml = glade_xml_new (DATADIR
			     "/nautilus/glade/sharing-properties-view.glade",
			     "content", NULL);

	g_return_val_if_fail (xml != NULL, NULL);

	view->vbox = glade_xml_get_widget (xml, "content");

	view->web_chk = glade_xml_get_widget (xml, "chk_sharing_web");
	view->web_lbl = glade_xml_get_widget (xml, "lbl_sharing_web");

	if (apache_is_running ()) {
		gtk_widget_set_sensitive (view->web_chk, TRUE);
		gtk_widget_hide (view->web_lbl);
	}
	else {
		gtk_widget_set_sensitive (view->web_chk, FALSE);
		gtk_label_set_text (GTK_LABEL (view->web_lbl), _("Can't detect any web server. You need one for web sharing"));
		gtk_widget_show (view->web_lbl);
	}

	return view;
}
