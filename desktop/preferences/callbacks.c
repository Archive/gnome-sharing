#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <gconf/gconf-client.h>

#include "callbacks.h"
#include "interface.h"
#include "helpers.h"
#include "support.h"

void
on_checkbutton1_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  GConfClient *client;
  gboolean active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton));

  gtk_widget_set_sensitive (lookup_widget(GTK_WIDGET(togglebutton), "label4"), active);
  gtk_widget_set_sensitive (lookup_widget(GTK_WIDGET(togglebutton), "label5"), active);
  gtk_widget_set_sensitive (lookup_widget(GTK_WIDGET(togglebutton), "entry2"), active);
  gtk_widget_set_sensitive (lookup_widget(GTK_WIDGET(togglebutton), "checkbutton2"), active);

  client = gconf_client_get_default ();
  gconf_client_set_bool (client, GCONFKEY, active, NULL);

  if(active)
    execute("gnome-desktop-sharing", FALSE);
}

void
on_checkbutton2_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  GConfClient *client;
  gboolean autoaccept;

  autoaccept = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton));
  client = gconf_client_get_default ();
  gconf_client_set_bool (client, GCONFKEY_AUTOACCEPT, autoaccept, NULL);
}

void
on_button1_activate                    (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_button3_activate                    (GtkButton       *button,
                                        gpointer         user_data)
{
}


void
on_button3_pressed                     (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *dialog;
	GConfClient *client;
	
	client = gconf_client_get_default ();
	gconf_client_set_string (client, GCONFKEY_PASSWD, gtk_entry_get_text(lookup_widget (GTK_WIDGET (button), "entry2")), NULL);
	dialog = lookup_widget(GTK_WIDGET(button), "window1");
	gtk_widget_destroy(dialog);
}


void
on_button3_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_window1_destroy                     (GtkObject       *object,
                                        gpointer         user_data)
{
	gtk_main_quit ();
}

