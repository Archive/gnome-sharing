#include <gtk/gtk.h>
#include "helpers.h"

void
execute (char *cmd, gboolean sync)
{
	gboolean retval;

	if (sync != FALSE)
		retval = g_spawn_command_line_sync
			(cmd, NULL, NULL, NULL, NULL);
	else
		retval = g_spawn_command_line_async (cmd, NULL);

	if (retval == FALSE)
	{
		char *msg;

		msg = g_strdup_printf
			("Couldn't execute command: %s",
			 cmd);

		g_message (msg);
		g_free (msg);
	}
}
