/* GNOME Desktop Sharing
 * Copyright (C) 2003 Rob Clews <robc@klearsystems.com>
 *
 * tray.h
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
 * USA.
 */

              
#ifndef __TRAY_H__
#define __TRAY_H__

#include <rfb/rfb.h>

#define GDS_GCONF_PATH "/apps/gnome-desktop-sharing/preferences"
#define GDS_GCONF_ENABLE GDS_GCONF_PATH"/enable"
#define GDS_GCONF_PASSWD GDS_GCONF_PATH"/password"
#define GDS_GCONF_AUTOACCEPT GDS_GCONF_PATH"/autoaccept"

void init_tray ();
void update_client_count(int client_count, rfbClientPtr client);

#endif
