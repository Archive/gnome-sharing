2004-01-31  Rodrigo Moya <rodrigo@gnome-db.org>

	* server/Makefile.am:
	* preferences/Makefile.am: s/GNETWORK/GNOME.

2004-01-30  Rodrigo Moya <rodrigo@gnome-db.org>

	Moved from gnome-network.

2004-01-29  Rob Clews  <robc@klearsystems.com>

	* server/Makefile.am:
	* server/dbus.c: (dbus_init_service), (server_unregistered_func),
	(server_message_func), (dbus_emit_signal):
	* server/dbus.h:
	* server/tray.c: (init_tray): Add D-BUS support, no signals are
	sent currently because I don't have D-BUS setup yet.

2004-01-28  Rob Clews <robc@klearsystems.com>

	* server/x11vnc.c: (client_gone), (new_client): Alert user when
	client connects or disconnects.

2004-01-28  Rob Clews  <robc@klearsystems.com>

	* preferences/callbacks.c: (on_checkbutton1_toggled),
	(on_checkbutton2_toggled):
	* preferences/callbacks.h:
	* preferences/helpers.h:
	* preferences/interface.c: (create_window1):
	* preferences/main.c: (main): add preferences option to automatically
	accept new clients

	* server/tray.h:
	* server/x11vnc.c: (cb_password_check), (new_client): use gconf
	setting to determine if we should automatically accept clients. If not
	use a dialog to ask

2004-01-27  Rob Clews <robc@klearsystems.com>

	* desktop/preferences/main.c: Show password when preferences dialog
	opens up

2004-01-06  Mason Kidd <mrkidd@mrkidd.com>

	* desktop/server/tray.c: Fix a segfault when opening/closing/
	reopening the 'Clients' dialog.  HIG fixes to the 'Clients' dialog.

2004-01-06  German Poo-Caaman~o <gpoo@ubiobio.cl>

	* server/tray.c: Fixed compilation warnings
	* server/x11vnc.c: Added initial NLS support

2004-01-01  Mason Kidd <mrkidd@mrkidd.com>

	* desktop/server/tray.c: modified popup menu to use GtkItemFactory
	and fixed About entry.

2004-01-01  Mason Kidd <mrkidd@mrkidd.com>

	* desktop/preferences/callbacks.c, helpers.h: Save password to gconf

	* desktop/server/tray.c, tray.h: Fix compilation problems with IP
	lookups, add defines for storing of password

	* desktop/server/x11vnc.c: Read password from gconf, and check against
	client supplied password
	
	* desktop/data/gnome-desktop-sharing.schemas.in: Add schema for
	password gconf entry

2003-11-26  Rodrigo Moya <rodrigo@gnome-db.org>

	* server/Makefile.am: use $VNC_CFLAGS, defined in configure.in.

2003-11-25  Rodrigo Moya <rodrigo@gnome-db.org>

	* Makefile.am:
	* server/Makefile.am: use correctly the conditional building stuff,
	and removed unused files.

2003-11-25  Rodrigo Moya <rodrigo@gnome-db.org>

	* data/Makefile.am: use $(srcdir) when running gconftool-2 on
	the schemas.

2003-11-06  German Poo-Caaman~o <gpoo@ubiobio.cl>
                                                                                
	* server/Makefile.am: Added conditional build.  It was necessary
      redefine LIBS by VNC_LIBS (or anythinrg else to avoid that
      automake complains about conditional definition of LIBS)
      This allow build gnome-network when libvncserver is not
      available.
                                                                                

2003-11-03  Rob Clews <robc@klearsystems.com>

	* server/tray.c: (lookup_ip), (update_client_count),
	(disconnect_clients), (create_model), (show_clients_dialog),
	(close_clients_dialog): Disconnect clients when selected


	* server/x11vnc.c: (client_gone), (new_client): Support
	more logins

2003-10-30  Rodrigo Moya <rodrigo@gnome-db.org>

	* server/tray.c (init_tray): use correct stock items for menu.

2003-10-30  Rob Clews <robc@klearsystems.com>

	* server/eggtrayicon.*: Update to latest
	* server/tray.c: Add & Remove nodes when connection/disconnecting
	* */.cvsignore: Ignore files

2003-10-29  Rodrigo Moya <rodrigo@gnome-db.org>

	* client/gnome-remote-desktop.desktop.in: don't include 'GNOME'
	in the application name, just 'Remote Desktop Client'.

2003-10-29  Rob Clews <robc@klearsystems.com>

	* server/*: started adding a dialog to show whose viewing your
	desktop and ability to disconnect them

2003-10-28  Rodrigo Moya <rodrigo@gnome-db.org>

	* Makefile.am: added data/ directory to build. Fixed generation
	of .desktop and .schema files.

2003-10-28  Rob Clews <robc@klearsystems.com>

	* server/tray.c: This fixes the bug when the preferences is
	run starting another process.

2003-10-28  Rob Clews <robc@klearsystems.com>

	* server/tray.h:
	* server/tray.c: Clean up code, remove useless includes

2003-10-28  Rob Clews <robc@klearsystems.com>

	* Makefile.am:
	* server/Makefile.am:
	* preferences/Makefile.am: fixed build for desktop sharing server.

	* preferences/interface.c: fixed typo.

	* server/tray.c: fixed email addresses.

2003-10-07  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop*:
	* main.c: removed gnome-remote-desktop sources, replaced with
	tsclient.

2003-09-24  Rodney Dawes  <dobey@free.fr>

	* gnome-remote-desktop.glade: Spacing fixes and make the dialog
	resizable
	
2003-09-19  Rodrigo Moya <rodrigo@gnome-db.org>

	* data/*:
	* server/*:
	* preferences/*: added gnome-remote-desktop-sharing sources from
	Rob Clews <rjc@kngs.bham.sch.uk>, sent by Erick Woods
	<erick@gnomepro.com>. Does not compile yet, hence not included in
	the build.

2003-08-25  William Jon McCann  <mccann@jhu.edu>

        * main.c (main): Added code to use default icon file.

2003-08-18  Álvaro Peña <apg@esware.com>

	* gnome-remote-desktop.glade: more HIG

	* main.c: Take some icons from the current gnome theme

2003-07-31  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop.png: added icon from apg.

	* Makefile.am:
	* gnome-remote-desktop.desktop.in: added the new icon.

2003-06-10  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop.glade: removed horizontal separators and placed
	connection settings before the method selector.

2003-06-06  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop.desktop: moved to .desktop.in.

	* Makefile.am: added correct @INTLTOOL_DESKTOP_RULE@.

2003-05-27  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop.glade: removed notebook and about page.

2003-02-15  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop.glade: set correct default option on radio
	buttons group.

2002-12-18  Rodrigo Moya <rodrigo@gnome-db.org>

	* gnome-remote-desktop.glade: added 'Display' options and redid the UI,
	to make it look much better.

	* main.c (load_dialog): get new widgets.
	(dialog_response_cb): take into account new widgets' values.

2002-12-17  Rodrigo Moya <rodrigo@gnome-db.org>

	* main.c (main) don't create the dialog...
	(load_dialog): ...load it from the Glade file.

	* gnome-remote-desktop.glade: added Glade file for main dialog
	interface.

	* Makefile.am: added installation of Glade files.

2002-12-13  German Poo-Caaman~o <gpoo@ubiobio.cl>

	* main.c: Fixed compilation warnings (mainly castings).

2002-12-12  Rodrigo Moya <rodrigo@gnome-db.org>

	* main.c (dialog_response_cb): if configure didn't find the
	VNC viewer program, display an error message instead of failing
	silently. Implemented Xnest frontend.

2002-12-12  Rodrigo Moya <rodrigo@gnome-db.org>

	* main.c (dialog_response_cb): implemented VNC viewing part.

2002-12-10  Rodrigo Moya <rodrigo@gnome-db.org>

	* Makefile.am: added missing file.

	* main.c (main): commented out the RFB server stuff and added
	implementation for very basic client.

2002-12-08  Rodrigo Moya <rodrigo@gnome-db.org>

	Added gnome-remote-desktop tool, a client/server for VNC (and others)

	* rfbserver.[ch]: RFB server implementation, for the time being, just
	a copy of libvncserver's x11vnc adapted for using GDK/GTK.

	* main.c: gnome-remote-desktop's main program.
